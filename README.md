Cartera de Inversiones
======================

Dominio del problema
--------------------

Fiubank ofrece a sus clientes diversas opciones de inversión entre las que se cuentan: plazo fijos, dolar y  acciones.

De acuerdo al sistema impositivo, las ganancias surgidas de operaciones financieras están sujetas a ciertos impuestos:

* Las operaciones de venta de dólares están sujetas a un impuesto del 1 % sobre la ganancia bruta de la operación. No se les aplica comisión.
* Las operaciones de plazo fijo no están sujetas a comisiones o impuestos particulares.
* Las operaciones de venta de acciones no están sujetas a impuestos pero si a comisiones, las cuales son de $ 10 para las operaciones menores a $ 5000 y de 3% para operaciones igual o mayores a $ 5000.


Adicionalmente, la ganancia financiera total se define como sumatoria de todos las inversiones del inversor (dolares, acciones y plazo fijo) y está sujeta sujeta a un impuesto progresivo distintos para personas físicas y empresas.

Para empresas:
* Si la ganancia bruta es menor o igual a 20.000, entonces la alícuota del impuesto es 0.
* Si la ganancia bruta es mayor a 20.000 y menor o igual 30.000 entonces la alícuota del impuesto es 5 %.
* Si la ganancia bruta es mayor a 30.000 y menor o igual 50.000 entonces la alícuota del impuesto es 15 %.
* Si la ganancia bruta es mayor a 50.000 la alícuota del impuesto es 35 %.

Para personas físicas:
* Si la ganancia bruta es menor o igual a 25.000, entonces la alícuota del impuesto es 0.
* Si la ganancia bruta es mayor a 25.000 entonces la alícuota es de 3 %

Nota: el cuit de las empresas comienza con 30 mientras que el de las personas físicas comienza con 20 o 27.

Se pide entonces construir un simulador de cartera de inversiones que provea la siguientes operaciones via una WebAPI:


Funcionalidades a desarrollar
------------------------------

### Agregar inversiones a la cartera

````
Request:
POST /inversiones/{cuit}/{tipo_inversion}
body
{ datos de la inversion }

Response: 
201 
{ identificador_unico_de_la_inversion, datos de la inversion}
````

### Consultar cartera de inversiones

```
Request:
GET /inversiones/{cuit}

Response: 
200 
{ [inversiones], ganancia_bruta, impuestos, ganancia_neta }

````

### Reset
Esta funcionalidad vuelve el sistema a cero y es usadada solo a fines de prueba.
```
Request
POST /reset 

Response
200
{ "resultado": "ok"}
```

Pendiente agregar mas casos.