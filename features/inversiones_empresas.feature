#language: es
Característica: Cartera de inversiones de empresa

  Antecedentes:
    Dado que mi cuit es "30445556667"

  @wip
  Esquema del escenario: Inversion solo en dolares
    Dado que compro $ <capital> dólares a cotizacion $ <cotizacion_que_compre>
    Cuando vendo esos dolares a cotización $ <cotizacion_que_vendi>
    Entonces obtengo una ganancia bruta de $ <ganancia_bruta>
    Y $ <impuestos_comisiones> de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ <ganancia_neta>

  Ejemplos:
    | capital | cotizacion_que_compre | cotizacion_que_vendi | ganancia_bruta | impuestos_comisiones |ganancia_neta |
    |    1000 |        80.5           |      90.0            |    9500.0      |         95.0         |    9405.0    |
    |    50000|        80.5           |      90.0            |    475000.0    |     171000.0         |    304000    |
    |    1000 |        80.5           |      101.0           |    20500.0     |       1230.0         |    19270.0   |
    |    1000 |        80.5           |      125.5           |    45000.0     |       7200.0         |    37800.0   |

  @wip
  Escenario: Inversion solo en plazo fijo
    Dado que el interes de plazo fijo es 10 % anual
    Cuando invierto $ 1000.0 en un plazo fijo a 365 dias
    Entonces obtengo una ganancia bruta de $ 100.0
    Y $ 0.0 de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ 100.0 

  @wip
  Escenario: Inversion solo en acciones
    Dado que compro $ 1000.0 en acciones a $ 50.0 por acción
    Cuando vendo esas acciones a $ 100.0 por acción
    Entonces obtengo una ganancia bruta de $ 1000.0
    Y $ 10.0 de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ 990.0

  @wip
  Escenario: Inversion solo en acciones con perdida
    Dado que compro $ 1000.0 en acciones a $ 50.0 por acción
    Cuando vendo esas acciones a $ 40.0 por acción
    Entonces obtengo una ganancia bruta de $ -200.0
    Y $ 10.0 de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ -210.0