#language: es
Característica: Cartera empresas

  Antecedentes:
    Dado que mi cuit es "30445556667"

  @wip
  Escenario: Inversiones varias en dolares con ganancia total mayor a $50.000
    Dado que compro $ 1000 dólares a cotizacion $ 80.5
    Cuando vendo esos dolares a cotización $ 90
    Y que compro $ 10000 dólares a cotizacion $ 100.0
    Cuando vendo esos dolares a cotización $ 200.0
    Entonces obtengo una ganancia bruta de $ 1009500.0
    Y $ 363420.0 de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ 646080.0

  @wip
  Escenario: Inversion en plazo fijo y acciones con ganancia total menor a $20.000
    Dado que el interes de plazo fijo es 10 % anual
    Y invierto $ 1000.0 en un plazo fijo a 365 dias
    Y que compro $ 1000.0 en acciones a $ 50.0 por acción
    Y vendo esas acciones a $ 100.0 por acción
    Entonces obtengo una ganancia bruta de $ 1100.0
    Y $ 10.0 de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ 1090.0
